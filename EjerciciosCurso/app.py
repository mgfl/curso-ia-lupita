from pickletools import optimize
from tabnanny import verbose
from numpy.core.numeric import True_
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np


celcius=np.array([-40,-10,0,8,15,22,38], dtype=float)
fa=np.array([-40,-14,32,46,49,72,100], dtype=float)

capa1=tf.keras.layers.Dense(units=3,input_shape=[1])
capa2=tf.keras.layers.Dense(units=3)
salida=tf.keras.layers.Dense(units=1)
modelo=tf.keras.Sequential([capa1,capa2,salida])

modelo.compile(
    optimizer=tf.keras.optimizers.Adam(0.1),
    loss='mean_squared_error'
)

print("entrenando")
historial=modelo.fit(celcius,fa,epochs=1000,verbose=True)
print ("Entrenamiento terminado.....")

plt.title('Proceso de perdidad Modelos')
plt.xlabel('epocas')
plt.ylabel("perdidas")
plt.plot(historial.history["loss"])
plt.show()

print("Probemos la IA")
TemperaturaPrueba=100
Resultado=modelo.predict([TemperaturaPrueba])
print("El resultado es:"+str(Resultado)+"F")